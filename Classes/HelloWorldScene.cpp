#include "HelloWorldScene.h"

USING_NS_CC;

/** InFile Method */

std::vector<char> GenerateListOperator() {
    std::vector<char> list = std::vector<char>();
    list.push_back('+');
    list.push_back('-');
    list.push_back('x');
    list.push_back(':');
    return list;
}

/** Header Method */

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::createWithPhysics();
    scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
    scene->getPhysicsWorld()->setGravity(Vec2(0,0));
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();
    layer->setPhysicWorld(scene->getPhysicsWorld());

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    // Setting Physicalworld;
    float answerHeight = 60;
    
    auto edgeNode = cocos2d::Node::create();
    edgeNode->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y + answerHeight + 500));
    auto edgeBody = cocos2d::PhysicsBody::createEdgeBox(Size(visibleSize.width*2,visibleSize.height + 1000));
    edgeBody->setCollisionBitmask(1);
    edgeBody->setContactTestBitmask(true);
    edgeNode->setPhysicsBody(edgeBody);
    this->addChild(edgeNode);
    
    // Add Collision Listener
    auto collisionListener = EventListenerPhysicsContact::create();
    collisionListener->onContactBegin = CC_CALLBACK_1(HelloWorld::onContactBegin, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(collisionListener, this);
    
    // Add Touch Listener
    auto touchListener = EventListenerTouchOneByOne::create();
    touchListener->onTouchBegan = CC_CALLBACK_1(HelloWorld::onTouchBegan, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
    
    this->generateAnswerButton();
    this->generateQuestionBox();
    
    return true;
}

void HelloWorld::update(float deltaTime) {
    CCLOG("Update with delta time: %f", deltaTime);
}

bool HelloWorld::onContactBegin(cocos2d::PhysicsContact &contact) {
    cocos2d::PhysicsBody *shapeABody = contact.getShapeA()->getBody();
    cocos2d::PhysicsBody *shapeBBody = contact.getShapeB()->getBody();
    
    cocos2d::PhysicsBody *questionBoxBody;
    
    if (shapeABody->getCollisionBitmask() == 2) {
        questionBoxBody = shapeABody;
    }

    if (shapeBBody->getCollisionBitmask() == 2) {
        questionBoxBody = shapeBBody;
    }
    
    if (questionBox != nullptr) {
        // Add Particle System
        cocos2d::ParticleExplosion *mEmitter = cocos2d::ParticleExplosion::create();
        mEmitter->setGravity(Vec2(0,-90));
        mEmitter->setPosition(questionBoxBody->getNode()->getPosition());
        this->addChild(mEmitter);
        
        questionBoxBody->getNode()->removeAllChildrenWithCleanup(true);
        questionBoxBody->getNode()->removeFromParentAndCleanup(true);
        questionBox = nullptr;
        sceneWorld->setSpeed(0);
        
        return false;
    }
    
    return true;
}

bool HelloWorld::onTouchBegan(cocos2d::Ref *pSender) {
    if (sceneWorld->getSpeed() <= 0) {
        if (questionBox != nullptr) {
            questionBox->removeAllChildrenWithCleanup(true);
            questionBox->removeFromParentAndCleanup(true);
            questionBox = nullptr;
        }
        
        sceneWorld->setSpeed(1);
        this->generateQuestionBox();
    }
    return true;
}

void HelloWorld::onTouchLeftButton(cocos2d::Ref *pSender) {
    if (questionBox==nullptr) {
        return;
    }
    
    if (leftLabel->getString() == std::to_string(results.back())) {
        CCLOG("Right choise");
        questionBox->removeAllChildrenWithCleanup(true);
        questionBox->removeFromParentAndCleanup(true);
        questionBox = nullptr;
        this->generateQuestionBox();
    } else {
        CCLOG("Fail");
        sceneWorld->setSpeed(0);
    }
}

void HelloWorld::onTouchRightButton(cocos2d::Ref *pSender) {
    if (questionBox==nullptr) {
        return;
    }

    
    if (rightLabel->getString() == std::to_string(results.back())) {
        CCLOG("Right choise");
        questionBox->removeAllChildrenWithCleanup(true);
        questionBox->removeFromParentAndCleanup(true);
        questionBox = nullptr;
        this->generateQuestionBox();
    } else {
        CCLOG("Fail");
        sceneWorld->setSpeed(0);
    }
}
/** Header Private Method */
std::string HelloWorld::generateQuestion() {
    std::string result = "";
    std::vector<char> list = GenerateListOperator();
    
    srand(time(0));
    int leftValue = rand() % 15;
    int operatorSymbol = rand() % list.size();
    int rightValue = 0;
    
    do {
        rightValue = rand() % 10;
    } while (rightValue == 0 && list.at(operatorSymbol) == ':');
    
    switch (list.at(operatorSymbol)) {
        case '+':
            this->results.push_back(leftValue + rightValue);
            break;
        case '-':
            this->results.push_back(leftValue - rightValue);
            break;
        case 'x':
            this->results.push_back(leftValue * rightValue);
            break;
        case ':':
            this->results.push_back(leftValue / rightValue);
            break;
        default:
            break;
    }
    
    result =  std::to_string(leftValue)
            + " "
            + list.at(operatorSymbol)
            + " "
            + std::to_string(rightValue);
    
    return result;
}

void HelloWorld::generateQuestionBox() {
    questionBox = cocos2d::Sprite::create();
    questionBox->setColor(Color3B::WHITE);
    questionBox->setAnchorPoint(Vec2(0.5,0));
    questionBox->setPosition(Vec2(this->visibleSize.width/2.0 + this->origin.x, this->visibleSize.height + this->origin.y));
    questionBox->setContentSize(Size(visibleSize.width,60));
    auto questionBoxPBody = cocos2d::PhysicsBody::createBox(questionBox->getContentSize(),PhysicsMaterial(0,0,100));
    questionBoxPBody->applyForce(Vec2(0,-100));
    questionBoxPBody->setCollisionBitmask(2);
    questionBoxPBody->setContactTestBitmask(true);
    questionBox->setPhysicsBody(questionBoxPBody);
    
    auto layerColor = cocos2d::LayerColor::create(Color4B::WHITE);
    layerColor->setContentSize(questionBox->getContentSize());
    layerColor->setPosition(0,0);
    questionBox->addChild(layerColor);
    
    auto lblText = cocos2d::Label::create();
    lblText->setString(this->generateQuestion());
    lblText->setPosition(questionBox->getContentSize().width/2.0, questionBox->getContentSize().height/2.0);
    lblText->setColor(Color3B::BLACK);
    lblText->setDimensions(questionBox->getContentSize().width, questionBox->getContentSize().height);
    lblText->setVerticalAlignment(cocos2d::TextVAlignment::CENTER);
    lblText->setHorizontalAlignment(cocos2d::TextHAlignment::CENTER);
    questionBox->addChild(lblText);

    this->addChild(questionBox);
    
    int result = this->results.back();
    if (rand()%2 == 0) {
        leftLabel->setString(std::to_string(result));
        rightLabel->setString(std::to_string(result - rand()%4-1));
    } else {
        rightLabel->setString(std::to_string(result));
        leftLabel->setString(std::to_string(result + rand()%4+1));
    }
}

void HelloWorld::generateAnswerButton() {
    auto menu = cocos2d::Menu::create();
    menu->setPosition(Vec2(0,0));
    menu->setAnchorPoint(Vec2(0,0));
    menu->setContentSize(Size(visibleSize.width,60));
    
    // Left Item
    auto leftItem = cocos2d::MenuItem::create(CC_CALLBACK_1(HelloWorld::onTouchLeftButton, this));
    leftItem->setPosition(Vec2(origin.x,origin.y));
    leftItem->setAnchorPoint(Vec2(0,0));
    leftItem->setContentSize(Size(visibleSize.width/2.0,60));
    
    auto leftLayerColor = cocos2d::LayerColor::create(Color4B::BLACK);
    leftLayerColor->setContentSize(leftItem->getContentSize());
    leftLayerColor->setPosition(Vec2(0, 0));
    leftLayerColor->setAnchorPoint(Vec2(0, 0));
    
    leftLabel = cocos2d::Label::create();
    leftLabel->setPosition(Vec2(leftItem->getContentSize().width/2.0, leftItem->getContentSize().height/2.0));
    leftLabel->setAnchorPoint(Vec2(0.5, 0.5));
    leftLabel->setString("LEFT");
    leftLabel->setColor(Color3B::WHITE);
    
    leftItem->addChild(leftLayerColor);
    leftItem->addChild(leftLabel);
    
    // Right Item
    auto rightItem = cocos2d::MenuItem::create(CC_CALLBACK_1(HelloWorld::onTouchRightButton, this));
    rightItem->setPosition(Vec2(origin.x + visibleSize.width/2.0,origin.y));
    rightItem->setAnchorPoint(Vec2(0,0));
    rightItem->setContentSize(Size(visibleSize.width/2.0,60));
    
    auto rightLayerColor = cocos2d::LayerColor::create(Color4B::WHITE);
    rightLayerColor->setContentSize(rightItem->getContentSize());
    rightLayerColor->setPosition(Vec2(0, 0));
    rightLayerColor->setAnchorPoint(Vec2(0, 0));
    
    rightLabel = cocos2d::Label::create();
    rightLabel->setPosition(Vec2(rightItem->getContentSize().width/2.0, rightItem->getContentSize().height/2.0));
    rightLabel->setAnchorPoint(Vec2(0.5, 0.5));
    rightLabel->setString("RIGHT");
    rightLabel->setColor(Color3B::BLACK);
    
    rightItem->addChild(rightLayerColor);
    rightItem->addChild(rightLabel);
    
    // Add To menu
    menu->addChild(leftItem);
    menu->addChild(rightItem);
    
    // Add to Scene
    this->addChild(menu);
}

