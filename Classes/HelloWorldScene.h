#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"

class HelloWorld : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
    
    void update(float deltaTime) override;
    void setPhysicWorld(cocos2d::PhysicsWorld* physicsWorld) { sceneWorld = physicsWorld; };
    
private:
    cocos2d::Label* leftLabel;
    cocos2d::Label* rightLabel;
    cocos2d::Sprite* questionBox;
    std::string generateQuestion();
    void generateQuestionBox();
    void generateAnswerButton();
    std::vector<int> results = std::vector<int>();
    cocos2d::PhysicsWorld *sceneWorld;
    cocos2d::Size visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
    cocos2d::Vec2 origin = cocos2d::Director::getInstance()->getVisibleOrigin();
    
    bool onContactBegin(cocos2d::PhysicsContact& contact);
    void onTouchLeftButton(cocos2d::Ref* pSender);
    void onTouchRightButton(cocos2d::Ref* pSender);
    bool onTouchBegan(cocos2d::Ref* pSender);
};

#endif // __HELLOWORLD_SCENE_H__
